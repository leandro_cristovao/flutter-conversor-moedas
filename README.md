# Conversor de moedas

Uma aplicação simples para conversão de moedas de forma offline

## Executar

Para rodar a aplicação, executar no terminal
        
        docker-compose build
        docker-compose up
    
Para acessar, digite no navegador a url http://localhost:4040/#/