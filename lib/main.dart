import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app/views/home_view.dart';

main() {
  runApp(AppWidget());
}

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              Color(0xFF00D953),
            ),
          ),
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Color(0xFF00D953),
        ),
        inputDecorationTheme: InputDecorationTheme(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF00D953),
            ),
          ),
          //Remove a borda de todos os TextField
          // border: InputBorder.none,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF00D953),
              width: 2,
            ),
          ),
        ),
        textTheme: ThemeData.dark().textTheme.apply(
              fontFamily: GoogleFonts.limelight().fontFamily,
            ),
      ),
      home: HomeView(),
    );
  }
}
