import 'package:conversor_moedas/app/components/currency_box.dart';
import 'package:conversor_moedas/app/controllers/home_controller.dart';
import 'package:conversor_moedas/app/models/currency_model.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late HomeController homeController;

  final TextEditingController toText = TextEditingController();
  final TextEditingController fromText = TextEditingController();

  @override
  void initState() {
    super.initState();
    homeController = HomeController(toText, fromText);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 30,
            right: 30,
            top: 100,
            bottom: 20,
          ),
          child: Column(
            children: [
              Image.asset('assets/logo.png'),
              Padding(
                padding: EdgeInsets.only(
                  bottom: 40,
                ),
              ),
              Text('Conversor de moedas burro para estudo do Flutter'),
              Padding(
                padding: EdgeInsets.only(
                  bottom: 80,
                ),
              ),
              CurrencyBox(
                controller: homeController.toText,
                selectedItem: homeController.toCurrency,
                items: homeController.currencies,
                onChanged: (value) {
                  setState(() {
                    if (value != null) homeController.toCurrency = value;
                  });
                },
              ),
              CurrencyBox(
                controller: homeController.fromText,
                selectedItem: homeController.fromCurrency,
                items: homeController.currencies,
                onChanged: (value) {
                  setState(() {
                    if (value != null) homeController.fromCurrency = value;
                  });
                },
              ),
              SizedBox(
                height: 50,
              ),
              ElevatedButton(
                onPressed: () {
                  homeController.convert();
                },
                child: Text(
                  'Converter',
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
